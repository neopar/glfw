# glfw

A collection of GLFW boilerplate projects.

## Dependencies

```
sudo apt install libglfw3 libglfw3-dev
```

## Quick Start

```
cd project_boilerplate
./build.sh
./build/name_of_executable
```
