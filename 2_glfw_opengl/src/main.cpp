#include <iostream>
#include "../lib/glad.h"
#include <GLFW/glfw3.h>

int main(void) {
	GLFWwindow* window;

	if (!glfwInit()) {
		std::cout << "ERROR: GLFW initialization failed." << std::endl;
		return -1;
	}

	window = glfwCreateWindow(800, 600, "Hello, GLFW!", NULL, NULL);
	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		std::cout << "ERROR: Failed to load OpenGL." << std::endl;
		glfwTerminate();
		return -1;
	}

	glClearColor(0.13f, 0.19f, 0.4f, 1.0f);

	while (!glfwWindowShouldClose(window)) {
		glfwPollEvents();
		glClear(GL_COLOR_BUFFER_BIT);
		glfwSwapBuffers(window);
	}

	glfwTerminate();

	return 0;
}
