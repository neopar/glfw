#include <iostream>
#include <GLFW/glfw3.h>

int main(void) {
	GLFWwindow* window;

	if (!glfwInit()) {
		std::cout << "ERROR: GLFW initialization failed." << std::endl;
		return -1;
	}

	window = glfwCreateWindow(800, 600, "Hello, GLFW!", NULL, NULL);
	glfwMakeContextCurrent(window);

	while (!glfwWindowShouldClose(window)) {
		glfwPollEvents();
		glfwSwapBuffers(window);
	}

	glfwTerminate();

	return 0;
}
